class RPNCalculator < Array

  def evaluate(string)

    arr = string.split(' ').map{ |el| el =~ /[0-9]/ ? el.to_i : el.to_sym }
    until arr.size == 1

      arr.each.with_index do |x,i|
        next if arr[i+2].is_a?(Integer)
        a = arr.delete_at(i)
        b = arr.delete_at(i)
        # return arr[i]
        case arr[i]
        when :+
          arr.insert(i, (a+b))
          arr.delete_at(i+1)
        when :-
          arr.insert(i, (a-b))
          arr.delete_at(i+1)
        when :*
          arr.insert(i, (a*b))
          arr.delete_at(i+1)
        when :/
          arr.insert(i, (a.to_f/b.to_f))
          arr.delete_at(i+1)
        end

        break
      end

    end
    arr.last
  end

  def tokens(string)
    string.split(' ').map{ |el| el =~ /[0-9]/ ? el.to_i : el.to_sym }
  end

  def plus
    raise "calculator is empty" if self.empty?
    b = self.pop
    a = self.pop
    self << (a.to_f + b)
  end

  def minus
    raise "calculator is empty" if self.empty?
    b = self.pop
    a = self.pop
    self << (a.to_f - b.to_f)
  end

  def times
    raise "calculator is empty" if self.empty?
    b = self.pop
    a = self.pop
    self << (a.to_f * b.to_f)
  end

  def divide
    raise "calculator is empty" if self.empty?
    b = self.pop
    a = self.pop
    self << (a.to_f / b.to_f)
  end

  def value
    self.last
  end

end
